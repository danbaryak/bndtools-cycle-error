package module1.impl;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

import module1.api.FirstService;
import module2.another.api.AnotherService;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		manager.add(createComponent()
				.setInterface(FirstService.class.getName(), null)
				.setImplementation(FirstServiceImpl.class)
				.add(createServiceDependency().setService(AnotherService.class).setRequired(true)));
		
	}

}
