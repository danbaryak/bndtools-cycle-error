package module1.impl;

import module1.api.FirstService;
import module2.another.api.AnotherService;

public class FirstServiceImpl implements FirstService {
	
	private volatile AnotherService anotherService;
	
	@Override
	public String sayHello() {
		return "well, hello world, universe and all that. value from another service is " + anotherService.sayAnotherThing();
	}

}
