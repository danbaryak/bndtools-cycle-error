package module2.another.impl;

import module2.another.api.AnotherService;

public class AnotherServiceImpl implements AnotherService {
	public String sayAnotherThing() {
		return "Another thing";
	}
}
