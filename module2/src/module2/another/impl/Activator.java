package module2.another.impl;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

import module2.another.api.AnotherService;

public class Activator extends DependencyActivatorBase{

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		manager.add(createComponent().setInterface(AnotherService.class.getName(), null)
				.setImplementation(AnotherServiceImpl.class));
		
	}

}
