package module2.impl;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

import module1.api.FirstService;
import module2.api.SecondService;


public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		manager.add(createComponent()
				.setInterface(SecondService.class.getName(), null)
				.setImplementation(SecondServiceImpl.class)
				.add(createServiceDependency().setService(FirstService.class).setRequired(true)));
	}

}