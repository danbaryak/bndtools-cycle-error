package module2.impl;

import module1.api.FirstService;
import module2.api.SecondService;

public class SecondServiceImpl implements SecondService {
	
	private volatile FirstService firstService;
	
	public void start() {
		System.out.println(firstService.sayHello());
	}
	
	public String saySomethingElse() {
		return "Something Else";
	}
}
